-- Adminer 4.8.1 PostgreSQL 11.22 (Ubuntu 11.22-1.pgdg22.04+1) dump

DROP TABLE IF EXISTS "Film";
CREATE TABLE "public"."Film" (
    "id" integer NOT NULL,
    "nom" character varying(128) NOT NULL,
    "description" character varying(2048) NOT NULL,
    "date" date NOT NULL,
    "note" smallint NOT NULL,
    CONSTRAINT "Film_id" PRIMARY KEY ("id")
);

INSERT INTO "Film" ("id", "nom", "description", "date", "note") VALUES
(1,	'Inception',	'Un thriller de science-fiction sur les rêves.',	'2010-07-16',	5),
(2,	'Forrest Gump',	'L''histoire d''un homme avec un QI inférieur à la moyenne qui réussit dans la vie.',	'1994-07-06',	4),
(3,	'The Dark Knight',	'Un film de super-héros basé sur le personnage de Batman.',	'2008-07-18',	5),
(4,	'Pulp Fiction',	'Une série d''histoires interconnectées dans le monde du crime.',	'1994-05-21',	4),
(5,	'La La Land',	'Une romance musicale sur deux aspirants artistes à Los Angeles.',	'2016-08-31',	3);

-- 2023-11-10 14:20:17.521347+01
